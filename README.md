# <center> Influências Ambientais em Saúde

## <center> Andre Marques, Denise Florio, Felipe Noronha, Izabela Fonseca e Nicolas Vana

Este repositório é referente ao projeto realizado na disciplina MAC0434/6967 - Laboratório Avançado de Ciências de Dados, oferecida pelo professor Fábio Kon do Departamento de Ciências da Computação do Instituto de Matemática e Estatística da Universidade de São Paulo durante o segundo semestre de 2020.

---

## Apresentação

O projeto desta equipe visa correlacionar aspectos ambientais com algumas doenças para gerarmos insights e se possível um modelo, visando facilitar e embasar o planejamento de ações e investimentos de órgãos governamentais. Em particular, temos as seguintes questões de pesquisa:

- É possível inferir causalidades em internações hospitalares a partir dos dados demográficos e de entorno do CENSO do IBGE?
- Quais doenças são decorrentes de interferências ambientais e urbanas na saúde?
- Quais são os fatores ambientais (tudo o que afeta o local que a pessoa mora, interno e externamente) que influenciam na causalidade das doenças?

Para viabilizar as análises desejadas, foram disponibilizados 19 arquivos contendo informações do IBGE (Censo 2010) de Domicílios e Entorno, trazendo informações sobre os municípios e seus moradores, e a base do Sistema de Informações Hospitalares (SIH) com informações de internações na rede pública dos estados da Bahia, Minas Gerais, Rio Grande do Sul e São Paulo, sobre pacientes e suas respectivas enfermidades devidamente classificadas de acordo com a base de Classificação Internacional de Doenças (CID10).

## Organização deste repositório

Abaixo temos a explicação de como este repositório é estruturado, descrevendo os diretórios principais:

- [dados](dados) - Todos os dados utilizados em nossas analises. Dentro das pastas `/CENSO`, `/SIH` e `/dados-clima` encontramos, respectivamente, os dados brutos do CENSO, do SIH e do INMET sobre o clima. O diretório `/intermediario` contem algumas planilhas auxiliares criadas como camadas intermediarias ate os datasets finais. Temos também a pasta `/CID10` e os arquivos `dicionario_*.pdf` que nos dão informações sobre as doenças e bases brutas que estamos trabalhando. Finalmente, temos os arquivos `*.csv` que são os datasets finais, gerados após nossa formatação, e que quando combinados da maneira devida ([olhe o notebook 'como-usar'](formatacao-dados/como_usar.ipynb)) constituem os dados utilizados em nossos modelos.
- [entregas](entregas) - Todas as entregas realizadas durante o decorrer da disciplina.
- [formatacao-dados](formatacao-dados) - Software utilizado para a formatação dos dados brutos do CENSO, SIH e Clima. Os _notebooks_ da pasta `/intermediarios` são responsáveis por juntar, limpar e normalizar as diferentes bases de dados do CENSO.
- [modelos](modelos) - _Notebooks_ que constituem os nossos modelos.
- [notebooks](notebooks) - _Notebooks_ antigos, que foram usados para as nossas analises exploratórias e testes de modelos.
- [referencias](referencias) - Links e artigos que foram e são importantes para o desenvolvimento do projeto.

## Como executar

Nessa sessão explicamos como executar o software que criamos, isso se divide nas duas etapas abaixo. Além disso, você vai precisar dos pacotes _Pandas_ e _Numpy_ do Python.

### Formatação de dados

Para criar os datasets finais vamos usar todos os _notebooks_ da pasta `formatacao-dados`. 

Primeiramente executamos os que estão dentro de `intermediários`, depois disso deveremos ter a pasta uma pasta com o mesmo nome dentro de `dados`.

Agora basta executar todos os _notebooks_ com nome `formatacao_*.ipynb`, com isso deveremos ter os respectivos arquivos criados dentro de `dados`.

Vale notar que se quisemos adicionar mais estados para nossa base, basta criamos um respectivo diretório dentro de `dados/CENSO` com as os arquivos `Domicilio01.csv`, `Domicilio02.csv`, `Entorno01.csv`, etc, daquele estado.

A mesma coisa vale para inclusão de outros anos de dados do SIH, basta incluir o arquivo com nome `rds_ANO.csv` dentro da pasta `dados/SIH`.

### Modelos

Os modelos consideraram a regressão linear multivariada, algoritmos de árvores de decisão (Light Gradient Boosting, Adaptative Gradient Boostin, eXtreme Gradiente Boosting, Random Forest) e redes neurais (ANN). 
Foram tomados os parâmetros obtidos dos dados de internação, nos estados de SP, RS, MG e BA, assim como os dados meteorológicos associados. Para efeitos de divisão entre grupos de treino e teste, adotou-se o percentual de 25%.
Os parâmetros foram analisados segundo sua importância, empregando-se as funções de "feature importance" dos algoritmos de árvores de decisão, de forma a otimizar o trabalho computacional. Os modelos consideraram os dados dos anos de 2010 a 2013 para o grupo de treino e os dados de 2014 como "teste". Uma variante foi também testada colocando-se o ano de menor quantidade de dados para teste e os demais (mais numerosos em dados de prevalência da doença) como "treino".

## Descrição dos dados

Utilizamos dados de 3 fontes, são elas:  

- Sistema de Informações Hospitalares do SUS (SIH-SUS), a partir do SIH conseguimos informações sobre atendimentos ambulatoriais na rede pública em todo o território nacional. Em particular, estamos olhando para o período de tempo que compreende os anos entre 2010 e 2014, devido a sua proximidade com o último Censo. 
- Censo de 2010 do IBGE, com mais de 1500 variáveis sobre os municípios e seus moradores, e dentre esses atributos conseguimos extrair dados sobre fatores ambientais e urbanísticos desses municípios. Ao todo, estamos analisando 2410 municípios, dos estados da Bahia, Minas Gerais, Rio Grande do Sul e São Paulo, ou cerca de 40% do total nacional. 
- Dados do INMET, coletados de 2010 a 2014, sobre variáveis climáticas de cada estado, como temperatura média, precipitação média entre outros. Decidimos usar estes dados devido ao seu caráter de controle, que permite um entendimento da situação meteorológica de um estado.

Com isso, juntamos essas 3 fontes em uma base única que possui 4 identificadores ─ município, doença, ano e mês (ou estação) ─ que mapeiam informações sobre o ambiente em um momento específico para uma determinada prevalência.

## O que foi tentado e não deu certo

Durantes as primeiras fases do projeto tentamos usar a seguinte abordagem: usar somente os dados do Censo e do SIH, realizando a agregação dos mesmos somente por município. isto é, nosso dataset final mapearia um município e uma doença para uma determinada prevalência (sem nenhuma informação sobre ano e mes/estação, e também sem nenhum tipo de variável de controle para o estado). Tentamos utilizar os mais diversos modelos na base com esse formato e os resultados eram sempre os mesmos, a previsão era muito ruim.

## O que deu certo

Sem duvida nenhuma, adicionar um maior grau de temporalidade na base ajuda bastante com a previsão do modelo. Adicionando ano e mes/estação na variáveis do dataset final fazemos com que o modelo consiga ter noção sobre tendencias de determinadas doenças, o que é fundamental.

Além disso, acreditamos que a inclusão de dados sobre o clima contribuiu positivamente para os resultados das previsões, pois estes dados atuam como variáveis de controle, informando para o modelo em qual estado um dado município se encontra.

Falando um pouco sobre os modelos testados, fizemos testes com modelos como regressão linear, redes neurais e algoritmos de árvores como Random Forest, XGBoost e LightGBM, onde este último foi o que apresentou melhores resultados até o momento. Próximos passos, tanto na melhoria da base de treino quanto na melhoria da performance dos modelos, serão abordadas na sessão seguinte. 


## Resultados

Foram desenvolvidos modelos com algoritmos de árvores de decisão, usando o LGBoost e XGBoost, os quais apresentaram resultados satisfatórios, com R2 próximo de 0.6. Comparando-se com uma métrica simples (média do valores de y_treino), ambos exibiram razões inferiores a 0.4. Uma razão igual a 1.0 significa que o resultado é igual à média dos valores de y_treino. Os modelos foram aplicados para várias doenças, como a pneumonia (devido a outros streptococos) a doença de Crohn, sendo possível identificar os municípios onde ocorreram. Isso foi feito para várias taxas de erro aceitáveis entre os dados de prevalência da doença e os valores preditos pelo XGBoost e LGBoost.

A doença que apresentou a melhor performance até então foi a “Pneumonia devida a outros tipos de estreptococos”, onde foi possível obter um R² de 0.85, que para este caso foi 62% melhor do que o R² da média. Além disso, observando a feature importance do modelo para esta doença, dentre as 20 variáveis mais importantes das 100 utilizadas no treinamento, observamos as seguintes variáveis:

- Domicílios com esgotamento em rio, lago ou mar.
- Existe arborização no entorno
- Não existe arborização no entorno
- Não existe calçada no entorno
- Moradores com esgotamento em rio, lago ou mar
- Número de homens
- Moradores com água de poço ou nascente
- Existe calçada no entorno

Tais variáveis são informações demográficas que aparentemente, de acordo com o modelo obtido, impactam na incidência dessa doença e que fazem sentido com o tipo da doença em questão que é uma doença respiratória (arborização) que é causada por estreptococos (esgotamento e fornecimento de água). Dessa forma, este modelo pode servir de embasamento para o Instituto Cordial ao apresentar argumentos aos órgãos governamentais de interesse estimativas de impacto de investimentos de infraestrutura das cidades nos gastos da saúde pública. 

Fazendo-se então uma simulação do modelo obtido de forma prescritiva, onde é feita uma alteração intencional em uma variável de interesse e o modelo treinado é reaplicado na base com tal alteração para entendermos como essa alteração impactaria no nosso resultado de interesse, reduzindo em 20% os valores da variável “Domicílios com esgotamento em rio, lago ou mar” que ocasionou uma redução na incidência de tal doença de 55 para 48 casos por 100 mil habitantes para as cidades-estação analisadas. Este resultado da aplicação prescritiva do modelo pode ser usado nos cálculos da análise financeira aproximada, podendo servir como um bom direcionamento de investimentos e esforços por parte dos órgãos governamentais de interesse. 

Finalmente, acreditamos que tais resultados foram satisfatórios e nos mostram que estamos no caminho para alcançarmos resultados cada vez melhores. Pensamos que ainda há a necessidade de se fazer uma análise mais profunda da influência das variáveis nos resultados bem como a exploração e aprimoramento de outros modelos e suas performances tanto para esta doença quanto para as demais. Além disso, a construção de novas variáveis a partir das já existentes, a obtenção de possíveis novas informações e o enriquecimento da base podem trazer melhorias significativas para os nossos resultados. 



